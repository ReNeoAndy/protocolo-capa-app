import socket
import getpass
from PIL import Image

def Main():
	'''Inicializa el cliente conectandose al puerto 9999 de la direccion IP proporcionada via terminal.
	En caso de error lanza una excepcion indicando el tipo de falla.
	'''

    host = input("Porfavor indique la IP del servidor: ")
    port = 9999
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    try:
        s.connect((host,port))
        s.settimeout(120)
        print("Conexión Establecida...")
        home(s)
        
    except Exception as e: 
        print("something's wrong with %s:%d. Exception is %s" % (host, port, e))


def home(s):
	''' Función principal que permite al usuario interactuar con el sistema, 
	se despliega un menu intercativo en el cual un usuario puede seleccionar
	iniciar sesion si ya esta registrado, registrarse si aun no lo ha hecho
	o salir del sistema.
	En caso de un error, el sistema arroja una excepcion mostrando el numero
	de error que ocurrio.
	'''

    estado = bytes([19])
    while(estado != bytes([100])):
        print("Porfavor ingrese el número de una de las siguientes opciones:\n 1-Iniciar Sesión\n 2-Crear cuenta\n 3-Salir")
        answer = input(" Elección: ")
        if(answer == "1"):
            mssg = bytes([10])
            print("\n\n----INICIO DE SESION----")
            user = input("Ingrese su correo: ").encode()
            password = getpass.getpass("Ingrese su contraseña: ").encode()
            mssg += user
            mssg += "|".encode()
            mssg += password
            s.send(mssg)
            respuesta = s.recv(100)
            #print(respuesta[0])
            byteMensaje = respuesta[0]
            if(byteMensaje == 15):
                print("¡¡¡Usuario o conrtaseña incorrectos!!!\n\n")
            else:
                id = respuesta[1]
                respuesta = respuesta[2:].decode()
                respuesta = respuesta.split("|")
                print("\n\n----SESIÓN INICIADA----")
                print("---User ID: " + str(id) + "---")
                sesionIniciada(respuesta, id, s)

        elif(answer == "2"):
            mssg = bytes([11])
            print("\n\n----REGISTRO DE USUARIO----")            
            user = input("Ingrese su correo: ").encode()
            password = getpass.getpass("Ingrese su contraseña: ").encode()
            imagen = input("Ingrese la ubicación de su imagen: ").encode()
            mssg += user
            mssg += "|".encode()
            mssg += password
            mssg += "|".encode()
            mssg += imagen
            s.send(mssg)
            respuesta = s.recv(1)
            #print(respuesta[0])
            byteMensaje = respuesta[0]
            if(byteMensaje == 16):
                print("¡¡¡ERROR 16: al registrar usuario!!!\n\n")
            elif(byteMensaje == 17):
                print("¡¡¡Por favor introduzca valores válidos!!!\n\n")
            else:
                print("\n\n---USUARIO AGREGADO---")
                print("---Por favor inicie sesión---")

        elif(answer == "3"):
            estado = bytes([100])
            s.send(estado)
        
        else:
            print("ERROR: Por favor ingrese una opción válida!!!")
    s.close()

def sesionIniciada(respuesta, id, s):
	'''Funcion que permite a un usuario registrado y que inicio sesion,
	poder subir archivos, ver sus archvios subidos, cerrar sesion.
	En caso de un error, el sistema arroja una excepcion mostrando el numero
	de error que ocurrio.
	'''
	
    correo = respuesta[0]
    imagen = respuesta[1]
    #print(imagen)
    estado = bytes([19])
    while(estado != bytes([25])):
        print("Ingrese el número de una de las siguientes opciones:\n 1-Subir un documento\n 2-Ver mis documentos\n 3-Ver imagen de pefil\n 4-Salir")
        answer = input("Elección: ")
        if(answer == "1"):
            print("\n\n---SUBIR ARCHIVO---")
            mssg = bytes([30])
            mssg += bytes([id])
            doc = input("Ingrese la ubicación de su archivo: ").encode()
            mssg += doc
            s.send(mssg)
            respuesta = s.recv(1024)
            if(respuesta[0] == 32):
                print("---Se ha subido tu archivo exitosamente!!!---\n\n")
            elif(respuesta[0] == 35):
                print("---ERROR 35: No se ha completado la subida de tu archivo!!!---\n\n")
            elif(respuesta[0] == 37):
                print("---ERROR 37: No tienes más capacidad de almacenamiento!!!---\n\n")

        elif(answer == "2"):
            print("\n\n---MIS ARCHIVOS---")
            mssg = bytes([40])
            mssg += bytes([id])
            s.send(mssg)
            respuesta = s.recv(1024)
            #print(respuesta)
            if(respuesta[0] == 44):
                print("---ERROR 44: Error al leer archivos!!!---\n\n")
            elif(respuesta[0] == 41):
                respuesta = respuesta[1:].decode()
                respuesta = respuesta.split("|")
                respuesta.remove("")
                #print(respuesta)
                while(len(respuesta) > 0):
                    idDoc = respuesta.pop(0)
                    print("-> ID Documento: " + idDoc)
                    doc = respuesta.pop(0)
                    print("-Documento: " + doc + "\n")

        elif(answer == "3"):
            im = Image.open(imagen)
            im.show()

        elif(answer == "4"):
            print("\n\n----SESIÓN CERRADA----")
            print("----Hasta luego!!! :)----\n\n")
            estado = bytes([25])

        else:
            print("ERROR: Por favor ingrese una opción válida!!!")
    return estado



if __name__ == "__main__":
    Main()