PYTHON = python3

.PHONY = help setup run clean 

.DEFAULT_GOAL = help

help:
	@echo "                HELP                "
	@echo "Para inicializar el proyecto escriba 'make setup'"
	@echo "Para instalar XAMPP escriba 'make xampp'"
	@echo "Para correr el proyecto escriba 'make run'"
	@echo "                "

setup:
	
	@echo "Instalando todo lo necesario..."
	@echo "Instalando 'python3'"
	sudo apt install python3
	@echo "Instalando php7.4"
	sudo apt install php7.4
	@echo "Instalando 'pip'"
	sudo apt-get install python3-pip
	@echo "Instalando 'Pillow'"
	sudo pip3 install Pillow
	@echo "Instalando 'mysql.connector'"
	sudo pip3 install mysql-connector-python
	@echo "¡Completado!"
	[ -f ReadMe.txt ] || (echo "Integrantes:\nGuevara Castro Miguel Andres\nRodríguez Romero Sergio Alfonso\nSánchez Segura Cristian Alonso\nLiga del repositorio: https://gitlab.com/ReNeoAndy/protocolo-capa-ap" >> ReadMe.txt)

xampp:
@echo "Descargando XAMPP"
	wget -r https://www.apachefriends.org/xampp-files/7.4.8/xampp-linux-x64-7.4.8-0-installer.run
	@echo "Instalando XAMPP"
	sudo chmod 755 ./www.apachefriends.org/xampp-files/7.4.8/xampp-linux-x64-7.4.8-0-installer.run
	sudo  ./www.apachefriends.org/xampp-files/7.4.8/xampp-linux-x64-7.4.8-0-installer.run
	sudo /opt/lampp/xampp start
	
run:
	gnome-terminal -- python3 server/multiserver.py
	gnome-terminal -- python3 client/client.py

clean:
	rm ReadMe.txt
