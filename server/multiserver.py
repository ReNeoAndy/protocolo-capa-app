import mysql.connector
import socket
import threading
import os


class ThreadedServer():
    '''Clase que implementa un servidor'''

    def __init__(self):
        '''Constructor de la clase'''
        self.host = '127.0.0.1'
        self.port = 9999
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) 
        self.s.bind(( self.host,self.port))                          
   
    def listen(self):
        '''Funcion que espera la conexion por parte de un cliente
        '''

        self.s.listen(5)
        print("Esperando conexiones...")
        while True:
            c, addr = self.s.accept()
            c.settimeout(120)
            threading.Thread(target = self.listenToClient(c,addr)).start()                                   

    def listenToClient(self, c, addr):
        '''Funcion que conecta a un cliente.

        Parametros:
        c -- Conector al socket
        addr -- Direccion IP del cliente
        '''

        print("Conectado desde: ", addr) 
        response = bytes([19])
        while(response != bytes([100])):
            response = c.recv(1024)
            if(response[0] == 10):
                aux = response[1:].decode()
                mssg = self.iniciaSesion(c, addr, aux)
                c.send(mssg)
            if(response[0] == 11):
                aux = response[1:].decode()
                mssg = self.registraUsuario(c, addr, aux)
                c.send(mssg)
            if(response[0] == 40):
                id = response[1]
                mssg = self.consultarArchivos(c, addr, id)
                c.send(mssg)
            if(response[0] == 30):
                id = response[1]
                #print(id)
                doc = response[2:].decode()
                #print(doc)
                mssg = self.subirArchivo(c, addr, id, doc)
                c.send(mssg)
        c.close()

    def iniciaSesion(self, c, addr, mssg):
        '''Funcion que permite el inicio de sesion para el cliente.

        Parametros:
        c -- Conector al socket
        addr -- Direccion IP del cliente
        mssg -- Mensaje que le indica que hacer al servidor
        '''

        mssg = mssg.split("|")
        user = mssg[0]
        password = mssg[1]
        #print("Iniciando sesión")
        #print(mssg)
        conn=mysql.connector.connect(host="localhost",user="root",passwd="",database="protocolo")
        db=conn.cursor()
        consulta = "SELECT * FROM `users` WHERE email='"+user+"' AND password='"+password+"';"
        db.execute(consulta)
        result = db.fetchall()
        if(len(result)>0):
            result = result[0]
            response = bytes([21])
            response += bytes([result[0]])
            response += result[1].encode()
            response += "|".encode()
            response += result[3].encode()
            #print(response)
            return response
        else:
            return bytes([15])

    def registraUsuario(self, c, addr, mssg):
        '''Funcion que permite el registro de un usuario.

        Parametros:
        c -- Conector al socket
        addr -- Direccion IP del cliente
        mssg -- Mensaje que le indica que hacer al servidor
        '''

        conn=mysql.connector.connect(host="localhost",user="root",passwd="",database="protocolo")
        db=conn.cursor()
        consulta = "SELECT * FROM users;"
        db.execute(consulta)
        result = db.fetchall() 
        ultimo = len(result)+1
        #print(ultimo)
        mssg = mssg.split("|")
        if(mssg[0] == "" or mssg[1]== ""):
            return bytes([17])
        else:
            try:
                consulta = "INSERT INTO `users` (`id`, `email`, `password`, `image`, `numDocs`) VALUES ('"
                consulta += str(ultimo)+"', '"
                consulta += mssg[0]+"', '"
                consulta += mssg[1]+"', '"
                consulta += mssg[2]+"', 0);"
                #print(consulta)
                db.execute(consulta)
                conn.commit()
                return bytes([27])
            except Exception as e:
                return bytes([16])
        
    def consultarArchivos(self, c, addr, id):
        '''Funcion que permite la consulta de archivos almacenados.

        Parametros:
        c -- Conector al socket
        addr -- Direccion IP del cliente
        id -- Identificador del usuario
        '''
        conn=mysql.connector.connect(host="localhost",user="root",passwd="",database="protocolo")
        db=conn.cursor()
        consulta = "SELECT idDoc, document FROM documents WHERE id='"
        consulta += str(id)+"';"
        #print(consulta)
        try:
            mssg = bytes([41])
            db.execute(consulta)
            result = db.fetchall()
            #print('----RESULTADO----')
            #print(result)
            cadena = ""
            for doc in result:
                for elem in doc:
                    cadena += str(elem)+"|"
            mssg += cadena.encode()
            #print(mssg)
            return mssg
        except Exception as e:
            #print("ERROR 44")
            return bytes([44])

    def subirArchivo(self, c, addr, id, doc):
        '''Funcion que permite cargar archivos a un usuario.

        Parametros:
        c -- Conector al socket
        addr -- Direccion IP del cliente
        id -- Identificador del usuario
        doc -- Nombre del documento o archivo que se va a cargar
        '''
        try:
            conn=mysql.connector.connect(host="localhost",user="root",passwd="",database="protocolo")
            db=conn.cursor()
            capacidad = "SELECT * FROM documents WHERE `id`= '" +str(id)+ "';"
            db.execute(capacidad)
            capacidadActual = db.fetchall()
            if(len(capacidadActual) >= 50):
                return bytes([37])
            else:
                consulta = "SELECT * FROM documents;"
                db.execute(consulta)
                result = db.fetchall() 
                ultimo = len(result)+1
                consulta = "INSERT INTO `documents`(`id`, `idDoc`, `document`) VALUES ('"
                consulta += str(id)+"', '"
                consulta += str(ultimo)+"', '"
                consulta += doc+"');"
                #print(consulta)
                db.execute(consulta)
                consulta2 = "SELECT numDocs FROM `users` WHERE `id`= '" +str(id)+ "';"
                db.execute(consulta2)
                result2 = db.fetchall() [0][0]
                #print(result2)
                result2 += 1
                consulta3 = "UPDATE `users` SET `numDocs` = '" +str(result2)+"' WHERE `users`.`id` = 2;"
                #print(consulta3)
                db.execute(consulta3)
                conn.commit()
                return bytes([32])
        except Exception as e:
            return bytes([35])

if __name__ == "__main__":
    '''Main
    Crea e inicializa la base de datos si aun no fue creada.
    '''
    conn=mysql.connector.connect(host="localhost",user="root",passwd="")
    db=conn.cursor()
    db.execute("show databases")
    lst=db.fetchall()
    data_name= "protocolo"
    if (data_name,) in lst:
        print("La base de datos ya existe")
    else:
        db.execute('''CREATE DATABASE protocolo;
        ''')
        db.execute(''' USE protocolo;
        ''')
        db.execute('''CREATE TABLE IF NOT EXISTS `users` (
                    `id` int(32) NOT NULL AUTO_INCREMENT,
                    `email` varchar(64) NOT NULL,
                    `password` varchar(64) NOT NULL,
                    `image` varchar(255) NOT NULL,
                    `numDocs` int(32) NOT NULL,
                    PRIMARY KEY (`id`)
                    ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;''')
        db.execute('''CREATE TABLE IF NOT EXISTS `documents` (
                    `id` int(11) NOT NULL,
                    `idDoc` int(11) NOT NULL,
                    `document` varchar(255) NOT NULL,
                    PRIMARY KEY (`idDoc`)
                    ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;''')
        print("Base de datos creada")
    ThreadedServer().listen()
